@extends('adminlte::page')

@section('title', 'Profil')

@section('content_header')
    
@stop

@section('content')
<br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3" style="display: flex; justify-content: space-between;">
                      <h1 class="text-dark">Data User</h1>
                      <a href="{{route('profil.edit', Auth::user()->id)}}" class="btn btn-primary mb-3">
                        Ubah Data
                      </a>
                    </div>
                    <table class="table table-hover table-bordered table-stripped" id="example2">
                        <tbody>
                            <tr>
                                <th>Nama</th>
                                <td>{{ Auth::user()->name }}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{ Auth::user()->email }}</td>
                            </tr>
                            
                        </tbody>
                    </table>
                    <br>
                    <h3>Event Terdaftar</h3>
                    <table class="table table-hover table-bordered table-stripped" id="example2">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Waktu</th>
                            <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($event as $key => $event)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$event->event->nama}}</td>
                                <td>{{$event->event->dt}}</td>
                                <td>
                                    <a href="{{route('events.show', $event->event_id)}}" class="btn btn-success btn-xs">
                                        Detail
                                    </a>
                                    <a href="{{route('profil.destroy', $event)}}" onclick="notificationBeforeDelete(event, this)" class="btn btn-danger btn-xs">
                                        Batal ikut
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    <form action="" id="delete-form" method="post">
        @method('delete')
        @csrf
    </form>
    <script>
        $('#example2').DataTable({
            "responsive": true,
        });

        function notificationBeforeDelete(event, el) {
            event.preventDefault();
            if (confirm('Apakah anda yakin akan menghapus data ? ')) {
                $("#delete-form").attr('action', $(el).attr('href'));
                $("#delete-form").submit();
            }
        }

    </script>
@endpush