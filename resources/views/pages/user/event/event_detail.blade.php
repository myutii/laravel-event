@extends('adminlte::page')

@section('title', 'Detail Event')

@section('content_header')
@stop

@section('content')
<br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3" style="display: flex; justify-content: space-between;">
                      <h1 class="text-dark">Detail Event</h1>
                      <form class="row g-3" action="/events/posts" method="post" enctype="multipart/form-data">
                      @csrf
                      <input type="hidden" id="id" name="id" value="{{$data->id}}">
                      <div class="col-md-12 mt-2">
                        <button type="submit" class="btn btn-primary">Join</button>
                      </div>
                    </form>
                    </div>                  
                    <table class="table table-hover table-bordered table-stripped" id="example2">
                        <img src="{{asset('storage/'.$data->foto)}}" class="mb-3 col-sm-3">
                        <tbody>
                            <tr>
                                <th>Nama Event</th>
                                <td>{{$data->nama}}</td>
                            </tr>
                            <tr>
                                <th>Tanggal Pelaksanaan</th>
                                <td>{{$data->dt}}</td>
                            </tr>
                            <tr>
                                <th>Deskripsi</th>
                                <td>{{$data->deskripsi}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop