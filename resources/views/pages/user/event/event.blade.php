@extends('adminlte::page')

@section('title', 'Data Event')

@section('content_header')
    
@stop

@section('content')
<br>
<div class="row">
@foreach($event_user as $key => $event)
  <div class="col-xl-3 col-md-4 col-sm-6">
    <div class="card overflow-hidden">
    <img src="{{asset('storage/'.$event->foto)}}" class="img-fluid img-thumbnail" alt="...">
      <div class="card-body">
      <a href="{{route('events.show', $event)}}"><h5 class="card-title">{{$event->nama}}</h5></a>
        <p class="card-text">{{$event->deskripsi}}</p>
        <form class="row g-3" action="/events/posts" method="post" enctype="multipart/form-data">
          @csrf
          <input type="hidden" id="id" name="id" value="{{$event->id}}">
        <div class="col-md-12 mt-2">
          <button type="submit" class="btn btn-primary">Join</button>
        </div>
      </form>
      </div>
    </div>
  </div>
@endforeach
</div>
@stop