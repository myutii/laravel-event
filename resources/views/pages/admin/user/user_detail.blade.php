@extends('adminlte::page')

@section('title', 'Detail User')

@section('content_header')
@stop

@section('content')
<br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3" style="display: flex; justify-content: space-between;">
                      <h1 class="text-dark">Detail User</h1>
                      <a href="/event" class="btn btn-secondary mb-3">
                        Kembali
                      </a>
                    </div>
                    <table class="table table-hover table-bordered table-stripped" id="example2">
                        <tbody>
                            <tr>
                                <th>Nama User</th>
                                <td>{{$data->name}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$data->email}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop