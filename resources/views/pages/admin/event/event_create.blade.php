@extends('adminlte::page')

@section('title', 'Laravel Event')

@section('content_header')
    <h1 class="m-0 text-dark">Data Event</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h1>
                      Tambah Data Event
                    </h1>
                    <div>
                        <form class="row g-3" action="/event/posts" method="post" enctype="multipart/form-data">
                            @csrf
                          <div class="col-md-12">
                            <label for="nama" class="form-label">Nama Event</label>
                            <input type="text" class="form-control @error ('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') }}">
                            @error('nama')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                          <div class="col-md-12">
                            <label for="foto" class="form-label">Gambar</label>
                            <img class="img-preview img-fluid mb-3 col-sm-3">
                            <input type="file" class="form-control @error ('foto') is-invalid @enderror" id="foto" name="foto" onchange="previewImage()">
                            @error('foto')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                          <div class="col-md-12">
                            <label for="dt" class="form-label">Tanggal</label>
                            <input type="datetime-local" class="form-control @error ('dt') is-invalid @enderror" id="dt" name="dt" value="{{ old('dt') }}">
                            @error('dt')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                          <div class="col-md-12">
                            <label for="deskripsi" class="form-label">Deskripsi</label>
                            <textarea class="form-control @error ('deskripsi') is-invalid @enderror" id="deskripsi" name="deskripsi" value="{{ old('deskripsi') }}"></textarea>
                            @error('deskripsi')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                          </div>
                          <div class="col-md-12 mt-2">
                            <button type="submit" class="btn btn-primary">Tambah</button>
                          </div>
                        </form>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
      function previewImage(){
        const foto = document.querySelector('#foto');
        const imgPreview = document.querySelector('.img-preview');

        imgPrewiew.style.display = 'block';

        conts oFReader = new FileReader();
        oFReader.readAsDataURL(foto.files[0]);

        oFReader.onload = function(oFREvent){
          imgPreview.src = oFREvent.target.result;
        }
      }
    </script>
@stop
