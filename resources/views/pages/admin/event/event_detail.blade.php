@extends('adminlte::page')

@section('title', 'Detail Event')

@section('content_header')
@stop

@section('content')
<br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3" style="display: flex; justify-content: space-between;">
                      <h1 class="text-dark">Detail Event</h1>
                      <a href="/event" class="btn btn-secondary mb-3">
                        Kembali
                      </a>
                    </div>
                    

                    <table class="table table-hover table-bordered table-stripped" id="example2">
                        <thead>
                            <tr>
                                <td colspan="2" align="center"></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Nama Event</th>
                                <td>{{$data->nama}}</td>
                            </tr>
                            <tr>
                                <th>Tanggal Pelaksanaan</th>
                                <td>{{$data->dt}}</td>
                            </tr>
                            <tr> 
                                <th>Foto</th>
                                <td>
                                    <img src="{{asset('storage/'.$data->foto)}}" class="mb-3 col-sm-3">
                                </td>
                            </tr>
                            <tr>
                                <th>Deskripsi</th>
                                <td>{{$data->deskripsi}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop