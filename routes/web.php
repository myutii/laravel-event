<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/welcome');
});

Auth::routes();

Route::get('/home',[App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('event', \App\Http\Controllers\EventController::class)->middleware(['auth','checkRole:2']);
Route::resource('/event/posts', EventController::class)->middleware(['auth','checkRole:2']);

Route::resource('user', \App\Http\Controllers\UserController::class)->middleware(['auth','checkRole:2']);


Route::resource('events', \App\Http\Controllers\EventUserController::class)->middleware(['auth','checkRole:1']);
Route::resource('/events/posts', \App\Http\Controllers\EventUserController::class)->middleware(['auth','checkRole:1']);

Route::resource('profil', \App\Http\Controllers\ProfilController::class)->middleware(['auth','checkRole:1']);