<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'foto',
        'nama',
        'dt',
        'deskripsi',
    ];

    public function userevent(){
        return $this->hasMany(UserEvent::class, 'event_id');
    }
}
