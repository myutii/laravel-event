<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facedes\Gate;
use Illuminate\Support\Facades\Storage;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = Event::all();

        return view('pages/admin/event/event', [
            'event' => $event
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/admin/event/event_create',[
            "title" => "Tambah Data Event",
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama'=>'required|min:5',
            'foto'=>'required|image|file|mimes:jpg,png,jpeg|max:2084',
            'dt'  =>'required',
            'deskripsi'=>'required',
        ],[
            'nama.required' => 'Nama event harus diisi',
            'foto.required' => 'Foto harus dipilih',
            'dt.required'   => 'Tanggal harus diisi',
            'deskripsi.required' => 'Deskripsi harus diisi',
            'nama.min'      => 'Nama event harus lebih dari 5 karakter',
            'foto.image'    => 'Foto harus berupa gambar',
            'foto.mimes'    => 'Foto harus berupa jpg/png/jpeg'
        ]);

        $validatedData['foto'] = $request->file('foto')->store('foto');

        Event::create($validatedData);

        return redirect()->route('event.index')->with('success_message', 'Berhasil menambah event baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('pages/admin/event/event_detail',[
            'data' => Event::find($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        if (!$event) return redirect()->route('event.index')
            ->with('error_message', 'Event dengan id'.$id.' tidak ditemukan');
        return view('pages/admin/event/event_update', [
            'event' => $event
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $request->validate([
            'nama' => 'required',
            'foto'=>'required|image|file|mimes:jpg,png,jpeg|max:2084',
            'dt'  =>'required',
            'deskripsi'=>'required',
        ],[
            'nama.required' => 'Nama event harus diisi',
            'foto.required' => 'Foto harus dipilih',
            'dt.required'   => 'Tanggal harus diisi',
            'deskripsi.required' => 'Deskripsi harus diisi',
            'nama.min'      => 'Nama event harus lebih dari 5 karakter',
            'foto.image'    => 'Foto harus berupa gambar',
            'foto.mimes'    => 'Foto harus berupa jpg/png/jpeg'
        ]);

        $event = Event::find($id);
        $event->nama = $request->nama;
        $event->dt = $request->dt;
        $event->deskripsi = $request->deskripsi;
        
        if($request->file('foto')){ 
            if($request->oldFoto){
                Storage::delete($request->oldFoto);
            }
            $event->foto = $request->file('foto')->store('foto');
        }
        
        $event->save();
        return redirect()->route('event.index')
            ->with('success_message', 'Berhasil mengubah data event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $event = Event::find($id);
        
        if($event->foto){
            Storage::delete($event->foto);
        }
        
        if ($event) $event->delete();
        return redirect()->route('event.index')
            ->with('success_message', 'Berhasil menghapus event');
    }
}
